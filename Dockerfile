FROM python:3.7

COPY requirements.txt /

RUN pip install -r /requirements.txt

COPY . /app

WORKDIR /app

RUN chmod +x entrypoint.sh

EXPOSE 8080

ENTRYPOINT ["/app/entrypoint.sh"]

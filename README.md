# README #

### What is this repository for? ###

*Development Lifecycle Demo*

*Task:* Utilize our sample application (th3-server.py inside th3.zip) and create a simple blue/green deployment strategy using the method/technology of your choosing. It is not required to have any endpoints exposed for us to examine but we would love to see your code and documentation in your Github repo. Feel free to use local resources (VMs, Minikube, etc.) to test and demonstrate your work. Using public cloud technology under their free tier is also fine!

### How did I do it? ###

* th3-server.py is baked into a docker image
* GCP CLoudRun service is used to run the docker image
* Build pipeline is set to build the docker image and deploy it to CloudRun

  * Step1
    
    **GIT** --CICD-Build-Pipeline--> **Build Docker Image** --> **Push to GCR**
    
  * Step2
    
    **GCR** --CICD-Build-Pipeline--> **Deploy to GCP CloudRun(new revision)**
    
**NOTE** 

To trigger the pipline you need to tag your commit as ``release-*``, `*` is a monotonically increasing number.

### Workflow ###

1. Add new features in code base
2. Tag your latest commit as `release-*`
3. Push the tag 
4. Build pipeline will be triggered.
   - Build pipeline builds a docker images with the latest changes and pushes it to GCR.
    - A new revision in CloudRun will be deployed with the `green` tag assigned but no traffic will be routed to it yet.
4. The ”green” tag allows us to directly test the new revision at a specific URL, without even needing to migrate traffic to it. As a developer, we can test the tagged revision at `https://green---CLOUDRUN_SERVICE_ENDPOINT`
5. After confirming that the new revision works properly, we can start migrating traffic to it:

`gcloud run services update-traffic SERVICE_NAME --to-tags green=50`

`gcloud run services update-traffic SERVICE_NAME --to-tags green=100`   

### Who do I talk to? ###

* your imaginary friend